/**
 * Created by kirill.vladykin on 07.07.2016.
 */

process.on("uncaughtException", function(err) {
    console.log("Error: ", err.message, err.stack);
});

const express = require("express");
const app = express();
const path = require("path");
const os = require("os");

function startPageMiddleware(req, res, next) {
    var form = "<script> function download() { window.location.href = 'download/' + document.getElementById('filename').value; } </script> <input id='filename' type='text' value='newfilename' /> <input type='button' value='download' onclick='download();' />";
    res.status(200).send(form);
}

function downloadFileMiddleware(req, res, next) {
    var fromFilename = path.normalize(__dirname + "/public/file.doc");
    var toFilename = req.params.filename + ".doc";

    console.log("File requested as " + toFilename + ", referrer = " + req.headers.referer);
    if (req.headers.referer) {
        res.set("Referer", req.headers.referer);
    }
    res.download(fromFilename, toFilename);
}

function pageNotFoundMiddleware(req, res, next) {
    res.status(404).send("Page not found");
}

app.get("/", startPageMiddleware); // start route for this task
app.get("/download/:filename", downloadFileMiddleware);
app.all("*", pageNotFoundMiddleware);

app.listen(1001);
console.log("Server is listening on port 1001");



